USE i_blok_library
GO
-- authors
ALTER TABLE authors ADD birthday DATE NULL,
    book_amount int NOT NULL DEFAULT (0) CHECK (book_amount>=0),
    issue_amount int NOT NULL DEFAULT (0) CHECK ( issue_amount >=0),
    total_edition int NOT NULL DEFAULT (0) CHECK (total_edition>=0)
GO
--booksauthors
--1
ALTER TABLE booksauthors 
DROP CONSTRAINT [FK_isbn]
GO
--2
ALTER TABLE books 
DROP CONSTRAINT [PK_books]
GO

ALTER TABLE books ADD title VARCHAR(255) NOT NULL DEFAULT ('Title'),
    edition int NOT NULL DEFAULT (1) CHECK (edition>=1),
    published DATE NULL,
    issue NUMERIC NULL
GO

--publisher
ALTER TABLE publisher ADD 
    created date NOT NULL DEFAULT ('01-01-1900'),
    country VARCHAR (255) NOT NULL DEFAULT ('USA'),
    city VARCHAR(255) NOT NULL DEFAULT('NY'),
    book_amount int NOT NULL DEFAULT (0) CHECK (book_amount>=0),
    issue_amount int NOT NULL DEFAULT (0) CHECK ( issue_amount >=0),
    total_edition int NOT NULL DEFAULT (0) CHECK (total_edition>=0)
GO

--authors_log

ALTER TABLE authors_log ADD 
    book_amount_old int NULL,
    issue_amount_old int NULL,
    total_edition_old int NULL,
    book_amount_new int NULL,
    issue_amount_new int NULL,
    total_edition_new int NULL
GO

select * from booksauthors
 
select * from books

select * from publisher

select * from authors